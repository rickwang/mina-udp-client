package com.cht.ami.udp.util;

public class Future {
	String id;
	Object object;

	public Future() {
	}
	
	public Future(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	@SuppressWarnings("unchecked")
	public synchronized <T> T getObject(long timeout) throws InterruptedException {
		if (object == null) {
			this.wait(timeout);
		}
		return (T) object;
	}

	public synchronized void setObject(Object object) {
		this.object = object;
		notifyAll();
	}
}
