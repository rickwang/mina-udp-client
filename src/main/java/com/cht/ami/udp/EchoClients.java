package com.cht.ami.udp;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.cht.ami.udp.util.Future;

import lombok.extern.slf4j.Slf4j;

@Profile("clients")
@Service
@Slf4j
public class EchoClients implements UdpBroker.Listener {
	@Value("${host:server}")
	String host;
	
	@Value("${port:12345}")
	int port;
	
	@Value("${clients.count:1}")
	int count;
	
	@Value("${clients.timeout:5000}")
	long timeout;
	
	@Value("${clients.resent-delay:1000}")
	long delay;

	@Autowired
	UdpBroker broker;
	
	ThreadPoolExecutor executor;
	ScheduledExecutorService scheduler;
	
	int tx = 0;
	int successed = 0;
	int timeouted = 0;
	int mismatched = 0;
	int unknown = 0;
	int idled = 0;
	
	@PostConstruct
	void start() throws Exception {
		executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(() -> {
			debug();
			
		}, 2, 2, TimeUnit.SECONDS);		
		
		executor.execute(() -> {
			fire();
		});
	}
	
	int lastTx = 0;
	int lastSuccessed = 0;
	long last = 0;
	
	void debug() {
		if (tx == lastTx) {
			return;
		}
		
		long now = System.currentTimeMillis();
		
		log.info(String.format("timeout: %,d, mismatch: %,d, unknown: %,d, success: %,d, ratio: %,d %%, rate: %,d /s, threads: %,d, sessions: %,d",
				timeouted, mismatched, unknown, successed,
				(successed - lastSuccessed) * 100 / (tx - lastTx),				
				(tx - lastTx) * 1000 / (now - last),
				executor.getActiveCount(),
				broker.connector.getManagedSessionCount()				
				));
		
		lastTx = tx;
		lastSuccessed = successed;
		last = now;			
	}
	
	// ======
	
	long seq = 0;
	
	synchronized String nextSeq() {
		return String.format("%016X", seq += 1);
	}
	
	void fire() {
		for (int i = 0;i < count;i++) {
			try {
				IoSession session = broker.connect(host, port);				
				
				executor.execute(() -> {
					fire(session);
				});
				
			} catch (Exception e) {
				log.error("Error", e);
			}
		}
	}
	
	void fire(IoSession session) {
		Future f = new Future();
		session.setAttribute("future", f);
		
		if (f != session.getAttribute("future")) {
			log.error("Shit Happen");
		}
		
		String seq = nextSeq();
		session.setAttribute("seq", seq);
		
		try {
			broker.send(session, seq.getBytes());
			tx += 1;
		
			String reply = f.getObject(timeout);
			if (reply != null) {
				if (reply.equals(seq)) {
					successed += 1;
					
				} else {
					mismatched += 1;
				}
				
			} else {
				timeouted += 1;				
			}
			
		} catch (Exception e) {
			log.error("Error", e);
			
		} finally {
			session.removeAttribute("future");
		}
		
		scheduler.schedule(() -> {			
			executor.execute(() -> {
				fire(session);
			});
			
		}, delay, TimeUnit.MILLISECONDS);
	}

	@Override
	public void onReceived(IoSession session, byte[] data) {		
		String reply = new String(data);
		
		Future f = (Future) session.getAttribute("future");
		if (f != null) {
			f.setObject(reply);
			
		} else {
			unknown += 1; // duplicated packets
		}
	}
	
	@Override
	public void onIdle(IoSession session) {
		idled += 1;
		
	}
	
	@Override
	public void onError(IoSession session, Throwable cause) {
		log.error("Error", cause);
		
	}
	
	@Override
	public void onClosed(IoSession session) {
		log.error("Closed");		
	}
}
