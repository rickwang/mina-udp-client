package com.cht.ami.udp;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.transport.socket.nio.NioDatagramConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Profile("clients")
@Service
@Slf4j
public class UdpBroker {
	@Value("${broker.processor-count:100}")
	int processors;
	
	@Value("${broker.timeout-in-seconds:5}")
	int timeout;
	
	IoConnector connector;
	
	@Autowired
	Listener listener;

	@PostConstruct
	void start() {
		connector = new NioDatagramConnector(processors);
		connector.setHandler(new UdpIoHandler());
		
		IoSessionConfig config = connector.getSessionConfig();
		config.setReaderIdleTime(timeout);
	}
	
	@PreDestroy
	void stop() {
		connector.dispose();
	}
	
	// ======
	
	public IoSession connect(String host, int port) throws InterruptedException {		
		ConnectFuture future = connector.connect(new InetSocketAddress(host, port));		
		
		return future.await().getSession();		
	}
	
	public void send(IoSession session, byte[] data) {
		IoBuffer buffer = IoBuffer.wrap(data);
		session.write(buffer);
	}
	
	class UdpIoHandler extends IoHandlerAdapter {

		@Override
		public void messageReceived(IoSession session, Object message) throws Exception {
			log.debug("Session recv...");
			
			IoBuffer buffer = (IoBuffer) message;
			
			byte[] bytes = new byte[buffer.remaining()];
			buffer.get(bytes);
			
			listener.onReceived(session, bytes);
		}

		@Override
		public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
			listener.onIdle(session);
		}
		
		@Override
		public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
			listener.onError(session, cause);
		}
		
		@Override
		public void sessionClosed(IoSession session) throws Exception {
			listener.onClosed(session);
		}
	}
	
	public static interface Listener {
		
		void onReceived(IoSession session, byte[] data);
		
		void onIdle(IoSession session);
		
		void onError(IoSession session, Throwable cause);
		
		void onClosed(IoSession session);
	}	
}
