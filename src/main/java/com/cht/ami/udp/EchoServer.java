package com.cht.ami.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Profile("server")
@Service
@Slf4j
public class EchoServer {
	@Value("${port:12345}")
	int port;

	DatagramSocket socket;
	
	int rx = 0;
	
	ScheduledExecutorService scheduler;
	
	@PostConstruct
	void start() throws Exception {
		socket = new DatagramSocket(port);
		new Thread(() -> {
			process();
			
		}).start();
		
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(() -> {
			debug();
			
		}, 2, 2, TimeUnit.SECONDS);		
	}

	long last = 0;
	int lastRx = 0;
	
	void debug() {
		long now = System.currentTimeMillis();
		
		log.info(String.format("rx: %,d, rate: %,d/s",				
				rx,
				(rx - lastRx) * 1000 / (now - last)));
		
		last = now;
		lastRx = rx;		
	}
	
	void process() {
		byte[] bytes = new byte[1500];
		
		try {
			for (;;) {
				DatagramPacket p = new DatagramPacket(bytes, bytes.length);
				
				socket.receive(p);
				
				rx += 1;
				
				socket.send(p);
			}		
			
		} catch (Exception e) {
			log.error("Error", e);
		}
	}
}
